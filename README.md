# Options

A small library to manage command line options and configuration parameters.

## Compiling

This library is written on Ada 2012 with no dependencies. You can compile it
with any suitable compiler. If you use GPR tools you can use it too.

## Command line parsing

Properties of options
- Multiple - option can take multiple values.

### Key and positional association

Options may Need Key.
When parsing the command line it should be always prepended by its key name.
If it is also hve a property "multiple" then every value should be prepended.

If the option does not need key, it can get a value without the need of a key.
Options not needing a key are associated with command line arguments in
order of their appearance.

Note: for options not needing a key there's no way to take
positional arguments past multiple option. So it should be the last one.