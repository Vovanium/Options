with Ada.Strings.Fixed;
use  Ada.Strings.Fixed;
with Ada.Command_Line;
use  Ada.Command_Line;

package body Options.Command_Line is

	Key_Prefix : constant String := "-";
	Infix      : constant String := "=";

	type Argument_Kind is (Value_Alone, Key_Alone, Key_Value);

	procedure Classify (
		Data        : in     String;
		Kind        :    out Argument_Kind;
		Key_First   :    out Positive;
		Key_Last    :    out Natural;
		Value_First :    out Positive)
	is
		I : Natural := Data'First;
		J : Natural;
	begin
		Value_First := Data'First;
		if Data'Length >= Key_Prefix'Length
		and then Data (I .. I + Key_Prefix'Length - 1) = Key_Prefix
		then
			I := I + Key_Prefix'Length;
			J := Index (Data, Infix, I);
			if J > I then
				Kind := Key_Value;
				Key_First := I;
				Key_Last := J - 1;
				Value_First := J + Infix'Length;
			else
				Kind := Key_Alone;
				Key_First := I;
				Key_Last := Data'Last;
				Value_First := Data'Last + 1;
			end if;
		else
			Kind := Value_Alone;
			Key_First := Data'First;
			Key_Last  := Data'First - 1;
			Value_First := I;
		end if;
	end;

	procedure Process (Opts : in out Option_Array) is
		Arg_I, Pos_I : Positive := 1;
		Opt_I : Positive := Opts'First;
		Was_Key : Boolean := False;
	begin

		while Arg_I <= Argument_Count loop
			declare
				A : String := Argument (Arg_I);
				K      : Argument_Kind;
				KF, VF : Positive;
				KL     : Natural;
				MT     : Integer;
			begin
				if Opts (Opt_I).all.Need_Value (With_Key => Was_Key) then
					K := Value_Alone;
					KF := A'First;
					KL := KF - 1;
					VF := KF;
				else
					Classify (A, K, KF, KL, VF);
					case K is
					when Value_Alone =>
						null;
					when Key_Alone =>
						MT := Find_Key (A (KF .. KL), Opts);
						if MT in Opts'Range then
							Opt_I := MT;
							Opts (MT).all.Accept_Key (A (KF .. KL));
						else
							K := Value_Alone;
							VF := A'First;
						end if;
					when Key_Value =>
						MT := Find_Key (A (KF .. KL), Opts);
						if MT in Opts'Range then
							Opts (MT).all.Accept_Key_Value (A (KF .. KL),
								A (VF .. A'Last));
						else
							K := Value_Alone;
							VF := A'First;
						end if;
					end case;
				end if;
				if K = Value_Alone then
					if Was_Key then
						if Opt_I in Opts'Range
						and then not Opts (Opt_I).all.Can_Accept_Value (With_key => True)
						then
							raise Constraint_Error with "Can not accept value " & A;
						end if;
					else
					while Opt_I in Opts'Range
					and then not Opts (Opt_I).all.Can_Accept_Value (With_Key => Was_Key)
					loop
						if Opt_I = Pos_I then
							Pos_I := Pos_I + 1;
						end if;
						Opt_I := Pos_I;
					end loop;
					end if;
					if Opt_I in Opts'Range then
						Opts (Opt_I).all.Accept_Value (A (VF .. A'Last));
					else
						raise Constraint_Error
						with "No suitable option for argument " & A;
					end if;
				end if;
				Was_Key := K = Key_Alone;
			end;
			Arg_I := Arg_I + 1;
		end loop;
	end;

end Options.Command_Line;
