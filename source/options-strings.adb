package body Options.Strings is

	function Key (Opt : Base_String_Option) return String
	is (Opt.Name);

	function Match (Opt : Base_String_Option; Key : String) return Boolean
	is (Key = Opt.Nm or else Key = Opt.Name);

	function Need_Value (Opt : Base_String_Option; With_Key : Boolean) return Boolean
	is (With_Key);

	function Can_Accept_Value (Opt : Base_String_Option; With_Key : Boolean) return Boolean
	is ((With_Key or not Opt.Need_Key) and (not Opt.Occured));

	procedure Accept_Value (
		Opt   : in out Base_String_Option;
		Value : in     String) is
	begin
		Opt.Occured := True;
	end Accept_Value;

	procedure Accept_Key_Value (
		Opt   : in out Base_String_Option;
		Key   : in     String;
		Value : in     String) is
	begin
		Option'Class (Opt).Accept_Key (Key);
		Option'Class (Opt).Accept_Value (Value);
	end Accept_Key_Value;

	function Usage_Placeholder (Opt : Base_String_Option) return String is ("*");

	overriding function Short_Usage (
		Opt        : Base_String_Option;
		Key_Prefix : String := "-";
		Infix      : String := "=")
		return String
	is (
		(if Opt.Need_Key then "" else "[") & Key_Prefix
		& (if Opt.Nm /= "" then Opt.Nm & "|" else "") & Opt.Name
		& Infix & (if Opt.Need_Key then "" else "]")
		& Usage_Placeholder (Base_String_Option'Class (Opt)));

	package body Makers is

		function Named (
			Name     : String;
			Need_Key : Boolean       := False)
			return     Base_String_Option
		is (Base_String_Option'(
			Nm_Length    => 0,
			Name_Length  => Name'Length,
			Nm           => "",
			Name         => Name,
			Need_Key     => Need_Key,
			Occured      => False));

		function Named (
			Nm,
			Name     : String;
			Need_Key : Boolean       := False)
			return     Base_String_Option
		is (Base_String_Option'(
			Nm_Length    => Nm'Length,
			Name_Length  => Name'Length,
			Nm           => Nm,
			Name         => Name,
			Need_Key     => Need_Key,
			Occured      => False));
	end Makers;

	function Make (
		Name     : String;
		Need_Key : Boolean       := False)
		return     String_Option
	is (Makers.Named (
		Name         => Name,
		Need_Key     => Need_Key)
		with others => <>);

	function Make (
		Nm,
		Name     : String;
		Need_Key : Boolean       := False)
		return     String_Option
	is (Makers.Named (
		Nm           => Nm,
		Name         => Name,
		Need_Key     => Need_Key)
		with others => <>);

end Options.Strings;
