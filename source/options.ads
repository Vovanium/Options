package Options with Preelaborate is

	type Option is limited interface;

	function Match (Opt : Option; Key : String) return Boolean is abstract;

	procedure Accept_Key (
		Opt : in out Option;
		Key : in     String) is null;
	-- Called externally when Key as a separate argument is supplied

	function Need_Value (Opt : Option; With_Key : Boolean) return Boolean is abstract;

	function Can_Accept_Value (Opt : Option; With_Key : Boolean) return Boolean is abstract;

	procedure Accept_Value (
		Opt   : in out Option;
		Value : in     String) is abstract;
	-- Called externally when value as a separate argument is supplied

	procedure Accept_Key_Value (
		Opt   : in out Option;
		Key   : in     String;
		Value : in     String) is abstract;
	-- Called when Key=value paie is called. Common behaviour is to call Accept_Key then Accept_Value

	function Short_Usage (
		Opt        : Option;
		Key_Prefix : String := "-";
		Infix      : String := "=")
		return String is abstract;

	type Option_Access is access Option'Class;

	type Option_Array is array (Positive range <>) of Option_Access;

	function Find_Key (Name : String; Opts : Option_Array) return Integer;

private

end Options;
