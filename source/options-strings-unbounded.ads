with Options.Strings.Generic_Values;
with Ada.Strings.Unbounded;
use  Ada.Strings.Unbounded;

package Options.Strings.Unbounded is
new Options.Strings.Generic_Values (
	Value_Type => Unbounded_String,
	To_Value   => To_Unbounded_String);
