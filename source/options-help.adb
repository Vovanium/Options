with Ada.Text_IO;
use  Ada.Text_IO;

package body Options.Help is

	procedure Print_Help (Opts : Option_Array) is
	begin
		for I in Opts'Range loop
			declare S : String := Opts (I).all.Short_Usage;
			begin
				if S /= "" then
					Put (Standard_Error, " " & S);
				end if;
			end;
		end loop;
		New_Line (Standard_Error);
	end;

end Options.Help;
