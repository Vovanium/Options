package Options.Triggers is

	type Trigger_Option (<>) is limited new Option with private;

	overriding function Match (
		Opt  : Trigger_Option;
		Key  : String)
		return Boolean;

	overriding procedure Accept_Key (
		Opt : in out Trigger_Option;
		Key : in     String);

	overriding function Need_Value (
		Opt      : Trigger_Option;
		With_Key : Boolean)
		return     Boolean;

	overriding function Can_Accept_Value (
		Opt      : Trigger_Option;
		With_Key : Boolean)
		return     Boolean;

	overriding procedure Accept_Value (
		Opt   : in out Trigger_Option;
		Value : in     String);

	overriding procedure Accept_Key_Value (
		Opt   : in out Trigger_Option;
		Key   : in     String;
		Value : in     String);

	overriding function Short_Usage (
		Opt        : Trigger_Option;
		Key_Prefix : String := "-";
		Infix      : String := "=")
		return String;

	function Make (
		Name     : String;
		Target   : access Boolean)
		return     Trigger_Option;

	function Make (
		Nm,
		Name     : String;
		Target   : access Boolean)
		return     Trigger_Option;

private

	type Trigger_Option (
		Nm_Length,
		Name_Length : Natural;
		Ref         : access Boolean)
	is limited new Option with record
		Nm       : String (1 .. Nm_Length);
		Name     : String (1 .. Name_Length);
	end record;

end Options.Triggers;
