package body Options.Strings.Multi is

	overriding function Can_Accept_Value (
		Opt      : Base_Multistring_Option;
		With_Key : Boolean)
		return     Boolean
	is (With_Key or not Opt.Need_Key);

	overriding function Short_Usage (
		Opt        : Base_Multistring_Option;
		Key_Prefix : String := "-";
		Infix      : String := "=")
		return String
	is (Base_String_Option (Opt).Short_Usage (Key_Prefix, Infix) & " ...");

end Options.Strings.Multi;
