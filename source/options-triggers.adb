package body Options.Triggers is

	overriding function Match (
		Opt  : Trigger_Option;
		Key  : String)
		return Boolean
	is (Key = Opt.Nm or else Key = Opt.Name);

	overriding procedure Accept_Key (
		Opt   : in out Trigger_Option;
		Key   : in     String) is
	begin
		Opt.Ref.all := True;
	end Accept_Key;

	overriding function Need_Value (
		Opt      : Trigger_Option;
		With_Key : Boolean)
		return     Boolean
	is (False);

	overriding function Can_Accept_Value (
		Opt      : Trigger_Option;
		With_Key : Boolean)
		return     Boolean
	is (False);

	procedure Accept_Value (
		Opt   : in out Trigger_Option;
		Value : in     String) is
	begin
		raise Constraint_Error with "Option value alone not allowed";
	end Accept_Value;

	procedure Accept_Key_Value (
		Opt   : in out Trigger_Option;
		Key   : in     String;
		Value : in     String) is
	begin
		if Value = "on" or else Value = "ON" or else Value = "On"
		or else Value = "true" or else Value = "TRUE" or else Value = "True"
		or else Value = "yes" or else Value = "YES" or else Value = "Yes"
		then
			Opt.Ref.all := True;
		elsif Value = "off" or else Value = "OFF" or else Value = "Off"
		or else Value = "false" or else Value = "FALSE" or else Value = "False"
		or else Value = "no" or else Value = "NO" or else Value = "No"
		then
			Opt.Ref.all := False;
		else
			raise Constraint_Error with "Unknown option " & Key & " value " & Value;
		end if;
	end Accept_Key_Value;

	overriding function Short_Usage (
		Opt        : Trigger_Option;
		Key_Prefix : String := "-";
		Infix      : String := "=")
		return String
	is (Key_Prefix & (if Opt.Nm /= "" then Opt.Nm & "|" else "") & Opt.Name
		& "(" & Infix & "on|off)");

	function Make (
		Name     : String;
		Target   : access Boolean)
		return     Trigger_Option
	is (Trigger_Option'(
		Nm_Length    => 0,
		Name_Length  => Name'Length,
		Ref          => Target,
		Nm           => "",
		Name         => Name));

	function Make (
		Nm,
		Name     : String;
		Target   : access Boolean)
		return     Trigger_Option
	is (Trigger_Option'(
		Nm_Length    => Nm'Length,
		Name_Length  => Name'Length,
		Ref          => Target,
		Nm           => Nm,
		Name         => Name));

end Options.Triggers;