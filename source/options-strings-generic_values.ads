generic
	type Value_Type is private;
	with function To_Value (S : String) return Value_Type;
	Placeholder : String := "*";
package Options.Strings.Generic_Values is

	type Value_Option is limited new Options.Strings.Base_String_Option with private;

	overriding procedure Accept_Value (
		Opt   : in out Value_Option;
		Value : in     String);

	overriding function Usage_Placeholder (Opt : Value_Option) return String;

	function Make (
		Name     : String;
		Object   : access Value_Type;
		Need_Key : Boolean       := False)
		return     Value_Option;

	function Make (
		Nm,
		Name     : String;
		Object   : access Value_Type;
		Need_Key : Boolean       := False)
		return     Value_Option;

private
	type Value_Option (
		Nm_Length,
		Name_Length  : Natural;
		Ref          : access Value_Type)
	is limited new Options.Strings.Base_String_Option (
		Nm_Length => Nm_Length,
		Name_Length => Name_Length) with null record;

end Options.Strings.Generic_Values;
