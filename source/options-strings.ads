package Options.Strings is

	type Base_String_Option (<>) is limited new Option with private;

	function Key (Opt : Base_String_Option) return String;

	overriding function Match (Opt : Base_String_Option; Key : String) return Boolean;

	overriding function Need_Value (Opt : Base_String_Option; With_Key : Boolean) return Boolean;

	overriding function Can_Accept_Value (Opt : Base_String_Option; With_Key : Boolean) return Boolean;

	overriding procedure Accept_Value (
		Opt    : in out Base_String_Option;
		Value  : in     String);

	overriding procedure Accept_Key_Value (
		Opt    : in out Base_String_Option;
		Key    : in     String;
		Value  : in     String);

	overriding function Short_Usage (
		Opt        : Base_String_Option;
		Key_Prefix : String := "-";
		Infix      : String := "=")
		return String;

	function Usage_Placeholder (
		Opt : Base_String_Option)
		return String;

	package Makers is
		function Named (
			Name     : String;
			Need_Key : Boolean       := False)
			return     Base_String_Option;

		function Named (
			Nm,
			Name     : String;
			Need_Key : Boolean       := False)
			return     Base_String_Option;
	end Makers;

	type String_Option is limited new Base_String_Option with private;

	function Make (
		Name     : String;
		Need_Key : Boolean       := False)
		return     String_Option;

	function Make (
		Nm,
		Name     : String;
		Need_Key : Boolean       := False)
		return     String_Option;

private
	type Base_String_Option (Nm_Length, Name_Length : Natural) is limited new Option with record
		Occured  : Boolean;
		Need_Key : Boolean;
		Nm       : String (1 .. Nm_Length);
		Name     : String (1 .. Name_Length);
	end record;

	type String_Option is limited new Base_String_Option with null record;

end Options.Strings;
