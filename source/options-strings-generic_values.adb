package body Options.Strings.Generic_Values is

	overriding procedure Accept_Value (
		Opt    : in out Value_Option;
		Value :  in     String)
	is
	begin
		Options.Strings.Base_String_Option (Opt).Accept_Value (Value);
		if Opt.Ref /= null then
			Opt.Ref.all := To_Value (Value);
		end if;
	end;

	overriding function Usage_Placeholder (Opt : Value_Option) return String is (Placeholder);

	function Make (
		Name     : String;
		Object   : access Value_Type;
		Need_Key : Boolean       := False)
		return     Value_Option
	is (Value_Option'(
		Nm_Length    => 0,
		Name_Length  => Name'Length,
		Nm           => "",
		Name         => Name,
		Need_Key     => Need_Key,
		Occured      => False,
		Ref          => Object));

	function Make (
		Nm,
		Name     : String;
		Object   : access Value_Type;
		Need_Key : Boolean       := False)
		return     Value_Option
	is (Value_Option'(
		Nm_Length    => Nm'Length,
		Name_Length  => Name'Length,
		Nm           => Nm,
		Name         => Name,
		Need_Key     => Need_Key,
		Occured      => False,
		Ref          => Object));

end Options.Strings.Generic_Values;
