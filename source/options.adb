package body Options is

	function Find_Key (Name : String; Opts : Option_Array) return Integer is
	begin
		for I in Opts'Range loop
			if Opts (I).all.Match (Name) then
				return I;
			end if;
		end loop;
		return Opts'First - 1;
	end Find_Key;

end Options;
