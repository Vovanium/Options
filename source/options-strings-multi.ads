package Options.Strings.Multi is

	type Base_Multistring_Option (<>) is limited new Base_String_Option with private;

	overriding function Can_Accept_Value (
		Opt      : Base_Multistring_Option;
		With_Key : Boolean)
		return     Boolean;

	overriding function Short_Usage (
		Opt        : Base_Multistring_Option;
		Key_Prefix : String := "-";
		Infix      : String := "=")
		return String;

private

	type Base_Multistring_Option (
		Nm_Length,
		Name_Length : Natural)
	is limited new Base_String_Option (
		Nm_Length   => Nm_Length,
		Name_Length => Name_Length)
	with null record;

end Options.Strings.Multi;
