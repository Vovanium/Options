with Options.Strings.Generic_Values;

package Options.Integers is new Options.Strings.Generic_Values (
	Value_Type => Integer,
	To_Value => Integer'Value,
	Placeholder => "<n>");
