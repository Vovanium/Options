with Ada.Strings.Unbounded, Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
use  Ada.Strings.Unbounded, Ada.Text_IO, Ada.Text_IO.Unbounded_IO;
with Options.Command_Line;
with Options.Strings.Unbounded;
with Options.Integers;
with Options.Triggers;
with Options.Help;

procedure Hello_World is

	package OUS renames Options.Strings.Unbounded;
	package OI  renames Options.Integers;
	package OT  renames Options.Triggers;

	Message     : aliased Unbounded_String := To_Unbounded_String ("Hello world!");
	Count       : aliased Integer := 1;
	Traditional,
	Help        : aliased Boolean := False;

	Opts : Options.Option_Array := (
		new OUS.Value_Option'(OUS.Make ("g", "greeting", Message'Access)),
		new OT.Trigger_Option'(OT.Make ("t", "traditional", Traditional'Access)),
		new OI.Value_Option'(OI.Make ("n", "count", Count'Access, Need_Key => True)),
		new OT.Trigger_Option'(OT.Make ("h", "help", Help'Access))
	);
begin
	Options.Command_Line.Process (Opts);
	if Help then
		Options.Help.Print_Help (Opts);
		return;
	end if;
	for I in 1 .. Count loop
		if Traditional then
			Put_Line ("Hello world!");
		else
			Put_Line (Message);
		end if;
	end loop;
end Hello_World;
