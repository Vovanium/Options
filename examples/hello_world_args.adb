with Ada.Text_IO;
use  Ada.Text_IO;

package body Hello_World_Args is

	procedure Accept_Key (
		Opt   : in out Hello_Option;
		Key   : in     String) is
	begin
		Put_Line (Opt.Key & "=" & Key);
		Base_String_Option (Opt).Accept_Key (Key);
	end Accept_Key;

	procedure Accept_Value (
		Opt   : in out Hello_Option;
		Value : in     String) is
	begin
		Put_Line (Opt.Key & ":" & Value);
		Base_String_Option (Opt).Accept_Value (Value);
	end Accept_Value;

end Hello_World_Args;
