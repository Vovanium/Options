with Options.Strings;
use  Options.Strings;

package Hello_World_Args is

	type Hello_Option is limited new String_Option with null record;

	overriding procedure Accept_Key (
		Opt   : in out Hello_Option;
		Key   : in     String);
	overriding procedure Accept_Value (
		Opt   : in out Hello_Option;
		Value : in     String);

end Hello_World_Args;
